import type { ConnectRouter, HandlerContext } from "@connectrpc/connect";
import { EduService } from "@buf/sphere_edu.connectrpc_es/edu/v1/edu_connect";
import {
  CreateUserRequest,
  CreateUserResponse,
  GetUserRequest,
  GetUserResponse,
} from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_pb";

const getUser = (rq: GetUserRequest): GetUserResponse => {
  return new GetUserResponse({
    user: {
      username: rq.id,
      fullName: rq.id + "'s full name would go here",
    },
  });
};

export default (router: ConnectRouter) =>
  router.service(EduService, {
    createUser: (req: CreateUserRequest): CreateUserResponse => {
      return new CreateUserResponse();
    },
    getUser: getUser,
  });
