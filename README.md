
To use:

```bash
npm install 
```

Installs:
```
# npm install typescript tsx
# npm install @bufbuild/buf @bufbuild/protoc-gen-es @bufbuild/protobuf @connectrpc/protoc-gen-connect-es @connectrpc/connect @buf/sphere_edu.connectrpc_es@latest
# npm install @buf/sphere_edu.bufbuild_es@latest
# npm install fastify @connectrpc/connect-node @connectrpc/connect-fastify
```

To run:

```bash
npx tsx server.ts
```

Client get:

```bash
curl --header 'Content-Type: application/json' --data '{"id": "Murphy"}' http://localhost:8080/edu.v1.EduService/GetUser
```

